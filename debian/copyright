Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Term-Shell
Upstream-Contact: Shlomi Fish <shlomif@shlomifish.org>
Source: https://metacpan.org/release/Term-Shell

Files: *
Copyright: 2001-2002, Neil Watkiss <NEILW@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright:
 2002-2007, Peter Makholm <peter@makholm.net>
 2002, Aurelien Jarno <aurel32@debian.org>
 2007-2021, gregor herrmann <gregoa@debian.org>
 2011, Ansgar Burchardt <ansgar@debian.org>
 2013, Florian Schlichting <fsfs@debian.org>
 2016, Lucas Kanashiro <kanashiro.duarte@gmail.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
